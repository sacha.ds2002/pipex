# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/02/21 13:20:16 by sada-sil          #+#    #+#              #
#    Updated: 2023/06/12 10:13:03 by sada-sil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = 			pipex

CC = 			gcc
CFLAGS = 		-Wall -Werror -Wextra
RM = 			rm -rf
LIBFT_PATH =	libft/libft.a

SRCS =			main.c \
				src/pipex_utils.c \
				src/ft_split_pipex.c

OBJS = $(SRCS:.c=.o)

${NAME}:	${OBJS}
	make libft.a -C libft
	${CC} ${CFLAGS} ${OBJS} -o ${NAME} ${LIBFT_PATH} 

all:	${NAME}

clean:
	${RM} ${OBJS}
	$(MAKE) clean -C ./libft

fclean: clean
	${RM} ${NAME}
	$(MAKE) fclean -C ./libft

re: fclean all

.PHONY: all clean fclean re