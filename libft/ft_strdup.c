/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/28 16:41:05 by sada-sil          #+#    #+#             */
/*   Updated: 2022/10/28 17:11:02 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	int		len;
	char	*s2;

	len = (int)ft_strlen(s1);
	if (s1[len] == '\0')
		len++;
	s2 = (char *)malloc(len * sizeof(char));
	if (!s2)
	{
		free(s2);
		return (NULL);
	}
	while (--len >= 0)
	{
		s2[len] = s1[len];
	}
	return (s2);
}
