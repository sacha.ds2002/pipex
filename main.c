/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/28 10:52:04 by sada-sil          #+#    #+#             */
/*   Updated: 2023/06/12 11:08:31 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

static void	parent_process_dups(int f2, int end[2])
{
	close(end[1]);
	dup2(end[0], STDIN_FILENO);
	dup2(f2, STDOUT_FILENO);
	close(end[0]);
	close(f2);
}

static void	child_process_dups(int f1, int end[2])
{
	close(end[0]);
	dup2(f1, STDIN_FILENO);
	dup2(end[1], STDOUT_FILENO);
	close(end[1]);
	close(f1);
}

int	pipex(int f1, int f2, char **av, char **env)
{
	int		id;
	int		end[2];
	char	**paths;

	paths = ft_get_paths(env);
	if (pipe(end) == -1)
		ft_throw_error("An error occured when opening the pipes.\n");
	id = fork();
	if (id == 0)
	{
		child_process_dups(f1, end);
		execute_cmd(av[2], paths, env);
	}
	else
	{
		parent_process_dups(f2, end);
		execute_cmd(av[3], paths, env);
	}
	return (0);
}

int	main(int ac, char **av, char **env)
{
	int	f1;
	int	f2;

	if (ac != 5)
		ft_throw_error("The program should have 4 parameters.\n");
	if (env == NULL)
		ft_throw_error("There is no environment, try restarting the shell.\n");
	f1 = open(av[1], O_RDONLY);
	f2 = open(av[4], O_CREAT | O_RDWR | O_TRUNC, 0644);
	if (f1 < 0 || f2 < 0)
		ft_throw_error("The files couldn't be opened.\n");
	pipex(f1, f2, av, env);
	return (0);
}
