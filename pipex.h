/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/28 10:53:05 by sada-sil          #+#    #+#             */
/*   Updated: 2023/04/26 11:36:10 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <errno.h>
# include "libft/libft.h"

char	**ft_get_paths(char **env);
char	**ft_split_pipex(char const *s, char c);
void	execute_cmd(char *filename, char **paths, char **env);
void	ft_throw_error(char *error);

#endif