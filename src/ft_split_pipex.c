/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_pipex.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/26 11:30:17 by sada-sil          #+#    #+#             */
/*   Updated: 2023/05/02 12:35:52 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../pipex.h"

static char	const	*ft_skip_chars(char const *s, char c, char *param_sep)
{
	while (*s && (*s == c || *s == '\'' || *s == '\"') && *param_sep == c)
	{
		if (*s == '\'' || *s == '\"')
			*param_sep = *s;
		s++;
	}
	return (s);
}

static int	ft_count_strs(char const *s, char c)
{
	int		nb;
	char	param_sep;

	nb = 0;
	param_sep = c;
	s = ft_skip_chars(s, c, &param_sep);
	while (*s)
	{
		nb++;
		while (*s && *s != param_sep)
			s++;
		param_sep = c;
		s = ft_skip_chars(s, c, &param_sep);
	}
	return (nb);
}

static int	ft_strlen_c(char const *s, char param_sep)
{
	int	i;

	i = 0;
	while (s[i] && s[i] != param_sep)
		i++;
	return (i);
}

static void	ft_free_arr(char **s, int i)
{
	while (i--)
		free(s[i]);
	free(s);
}

char	**ft_split_pipex(char const *s, char c)
{
	char	**result;
	int		count;
	int		i;
	char	param_sep;

	count = ft_count_strs(s, c);
	result = (char **)ft_calloc(sizeof(char *), count + 1);
	if (!result)
		return (0);
	i = 0;
	while (i < count)
	{
		param_sep = c;
		s = ft_skip_chars(s, c, &param_sep);
		result[i] = ft_strndup(s, ft_strlen_c(s, param_sep));
		if (!result)
		{
			ft_free_arr(result, i - 1);
			return (0);
		}
		s += ft_strlen_c(s, param_sep);
		i++;
	}
	return (result);
}
