/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/04/14 11:41:38 by sada-sil          #+#    #+#             */
/*   Updated: 2023/06/12 11:08:26 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../pipex.h"

static char	**ft_parse_params(char *str)
{
	if (ft_strchr(str, '\'') || ft_strchr(str, '\"'))
		return (ft_split_pipex(str, ' '));
	else
		return (ft_split(str, ' '));
}

void	execute_cmd(char *str, char **paths, char **env)
{
	char			*cmd;
	char			**cmdargs;
	unsigned long	i;

	cmdargs = ft_parse_params(str);
	if (str[0] == '\0' || !str)
		ft_throw_error("Empty parameter.");
	i = -1;
	while (++i < sizeof(paths) - 1)
	{
		cmd = ft_strjoin(paths[i], cmdargs[0]);
		if (access(cmd, X_OK) == 0)
		{
			execve(cmd, cmdargs, env);
			free(cmd);
			break ;
		}
		free(cmd);
	}
	ft_throw_error("Command not found.");
}

char	**ft_get_paths(char **env)
{
	char	*tmp;
	char	**ret;
	int		i;

	while (!ft_strnstr(*env, "PATH", 4))
		env++;
	tmp = ft_substr(*env, 5, ft_strlen(*env) - 5);
	ret = ft_split(tmp, ':');
	i = -1;
	while (ret[++i] != NULL)
		ret[i] = ft_strjoin(ret[i], "/");
	free(tmp);
	return (ret);
}

void	ft_throw_error(char *error)
{
	ft_putstr_fd(error, STDERR_FILENO);
	exit(1);
}
